# Laboratorio 5: Templates

## Estudiantes:
```
Roberto Acevedo Mora
Alexander Calderón Torres
```

## Instrucciones de compilación:
Una vez descargado el código, ubíquese en la respectiva carpeta:
```
>>cd {PATH}/lab5
```
Seguidamente, ejecute el make:
```
>>make
```

## Para ejecutar el programa:
Para realizar operaciones con fracciones:
```
./bin/calculadora a / b % d / e 
```
donde: a, b, d y e se sustituyen por números reales
	   % se sustituye por una de las siguientes operaciones:
	   		+ : Suma
	   		- : Resta
	   		/ : División
	   		= : Igualdad
	   		x : Multiplicación (NO USAR EL SÍMBOLO *)


Si lo desea, puede correr algunos cálculos de prueba predefinidos, con el comando:
```
make test
```
## Para generar la documentación de Doxygen:
Ubíquese en la carpeta principal:
```
>>cd {PATH}/lab5
```

Compile el archivo Doxyfile:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd docs/latex
>>make
```
Esto generará el archivo refman.pdf con la documentación generada por doxygen.
