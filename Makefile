TARGET = calculadora

CCX = g++
CFLAGS = -Wall -g

LINKER = g++
LFLAGS = -I.

SRCDIR = src
OBJDIR = build
BINDIR = bin
INCDIR = include

SOURCES := $(wildcard $(SRCDIR)/*.cpp)
INCLUDES := $(wildcard $(INCDIR)/*.h)
OBJECTS := $(SOURCES:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
rm = rm -f


all: $(BINDIR)/$(TARGET)


$(BINDIR)/$(TARGET): $(OBJECTS)
	@$(LINKER) $(OBJECTS) $(LFLAGS) -o $@
	@echo "Linker done"


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.cpp
	@$(CCX) $(CFLAGS) -I$(INCDIR) -I$(SRCDIR) -c $< -o $@
	@echo "Compiling done"


clean:
	@$(rm) $(OBJECTS)
	@$(rm) $(OBJDIR)/*.o
	@$(rm) $(BINDIR)/$(TARGET)


.PHONY: all clean

test:
	@echo ""
	@echo "----------------EJEMPLOS DE FUNCIONAMIENTO----------------"
	@echo ""
	@echo "Suma de polinomios:"
	@echo ">>  x^2 + 2x + 3 + 4x^2 + 5x + 6"
	@./bin/calculadora 1 2 3 + 4 5 6
	@echo ""
	@echo "Resta de polinomios:"
	@echo ">>  (x^2 + 2x + 3) - (4x^2 + 5x + 6)"
	@./bin/calculadora 1 2 3 - 4 5 6
	@echo ""
	@echo "Multiplicación de polinomios:"
	@echo ">>  (x^2 + 2x + 3) * (4x^2 + 5x + 6)"
	@./bin/calculadora 1 2 3 x 4 5 6
	@echo ""
	@echo "División de polinomios:"
	@echo ">>  (x^2 + 2x + 1) / (x + 1)"
	@./bin/calculadora 1 2 1 / 0 1 1
	@echo ""
	@echo "Igualar dos polinomios:"
	@echo ">>  (x^2 + 2x + 3) = (4x^2 + 5x + 6)"
	@./bin/calculadora 1 2 3 = 4 5 6
	@echo ""
	@echo ""
	@echo "Suma de fracciones:"
	@echo ">>  3/5 + 8/9"
	@./bin/calculadora 3 / 5 + 8 / 9
	@echo ""
	@echo "Resta de fracciones:"
	@echo ">>  1/2 - 3/1"
	@./bin/calculadora 1 / 2 - 3 / 1
	@echo ""
	@echo "Multiplicación de fracciones:"
	@echo ">>  10/4 * 4/2"
	@./bin/calculadora 10 / 4 x 4 / 2
	@echo ""
	@echo "División de fracciones:"
	@echo ">>  305/5 / 900/300"
	@./bin/calculadora 305 / 5 / 900 / 300
	@echo ""
	@echo "Igualar dos fracciones:"
	@echo ">>  1 / 1 = 2.5/0.5"
	@./bin/calculadora 1 / 1 = 2.5 / 0.5