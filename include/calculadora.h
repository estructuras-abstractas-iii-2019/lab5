#pragma once

/**
 * @author Alexander Calderón Torres
   @author Roberto Acevedo
 * @date 06/02/2020
 * @file calculadora.h
 * @brief Aqui se define la estructura de la clase Calculadora.
 */

#include <iostream>

template <typename Data> 


/**
 *  @class Calculadora
 *  @brief Clase emplantillada.
 */

class Calculadora
 {
 public:
	
	  /**
       * @brief Constructor 
       */

 	Calculadora();

	 /**
       * @brief Destructor
       */
 	~Calculadora();
	
	/**
       * @brief Funcion add que retorna la suma de los parametros
	   * @param &d1 referencia de parametro
       * @param &d2 referencia de parametro constante
       */
 	Data add(Data &d1, const Data &d2);

	/**
       * @brief Funcion sub que retorna la resta de los parametros
	   * @param Data &d1 referencia de parametro
       * @param Data &d2 referencia de parametro constante
       */
 	Data sub(Data &d1, const Data &d2);

	/**
       * @brief Funcion sub que retorna la multiplicacion de los parametros
	   * @param Data &d1 referencia de parametro
       * @param Data &d2 referencia de parametro constante
       */
 	Data mul(Data &d1, const Data &d2);

	/**
       * @brief Funcion div que retorna la division de los parametros
	   * @param Data &d1 referencia de parametro
       * @param Data &d2 referencia de parametro constante
       */
 	Data div(Data &d1, const Data &d2);

	/**
     * @brief Funcion que imprime los resultados
	 * @param Data &d1 referencia de parametro
     */
	
 	void print(Data &d1);
 };

 #include "calculadora.tpp"