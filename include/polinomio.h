#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo 
 * @date 06/02/2020
 * 
 * @file polinomio.h
 * @brief Aqui se define la estructura de la clase Polinomio.
 */

#include "calculadora.h"

using namespace std;

/**
 * @class Polinomio
 * @brief Define las sobrecargas de operadores e incluye elementos privados encargados de almacenar
 * los datos correspondientes al polinomio ingresado (A B C), el cociente, el residuo.
 */

class Polinomio{

public:

	 /**
      * @brief Constructor, inicializa el polinomio.
      * 
      * @param a Primer elemento.
      * @param b Segundo elemento.
      * @param c Tercer elemento.
      */
	Polinomio(float a, float b, float c);
	
	 /**
      * @brief Destructor.
      */
	
	~Polinomio();

	 /**
      * @brief Sobrecarga del ooperador +
      * 
      * @param frac Objeto tipo polinomio.
      */

	void operator+(const Polinomio &frac);

	
	 /**
      * @brief Sobrecarga del ooperador *
      * 
      * @param frac Objeto tipo polinomio.
      */
	void operator*(const Polinomio &frac);

	
	 /**
      * @brief Sobrecarga del ooperador /
      * 
      * @param frac Objeto tipo polinomio.
      */
	void operator/(const Polinomio &frac);

	
	 /**
      * @brief Sobrecarga del ooperador -
      * 
      * @param frac Objeto tipo polinomio.
      */
	void operator-(const Polinomio &frac);

	/**
     * @brief Igualdad entre los dos parametros 
     * 
     * @param frac Objeto tipo fracción.
     */

	Polinomio& operator=(const Polinomio &frac);

	 /**
      * @brief imprime los resultados de las operaciones
      */
	void imprimir();

	 /**
      * @brief Retorna los valores de los valores float a,b y c
      */

	float getA();
	float getB(); 
	float getC();

private:
	float a, b, c, d, e, f, g, resultado[5];
};