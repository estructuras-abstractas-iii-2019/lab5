#pragma once

/**
 * @author Alexander Calderón Torres
 * @author Roberto Acevedo
 * @date 06/02/2020
 * 
 * @file fraccion.h
 * @brief En este archivo se definen los métodos de la clase Fraccion.
 */

#include "calculadora.h"

using namespace std;

/**
 * @class Fraccion
 * @brief La clase Fracción definen las distintas sobrecargas de operadores necesarias para la implementación del programa.
 * Define elementos privados que almacenan los numeradores y el denominador que se imprimirán posteriormente.
 */

class Fraccion
{
public:

	 /**
      * @brief Constructor por defecto que recibe los parámetros del usuario.
      * 
      * @param num Primer parámetro.
      * @param den Segundo parámetro.
      */
	Fraccion(float num, float den);

	/**
     * @brief Destructor.
     */  
	~Fraccion();

	 /**
      * @brief Sobrecarga del operador +
      * 
      * @param frac Objeto tipo fracción.
      */

	void operator+(const Fraccion &frac);

	/**
     * @brief Sobrecarga del operador *
     * 
     * @param frac Objeto tipo fracción.
     */
	void operator*(const Fraccion &frac);

	 /**
      * @brief Sobrecarga del operador /
      * 
      * @param obj Objeto tipo fracción.
      */ 
	void operator/(const Fraccion &frac);

	/**
     * @brief Sobrecarga del operador -
     * 
     * @param frac Objeto tipo fracción.
     */
	void operator-(const Fraccion &frac);

	/**
     * @brief Igualdad entre los dos parametros 
     * 
     * @param frac Objeto tipo fracción.
     */

	Fraccion& operator=(const Fraccion &frac);

	/**
     * @brief imprime los resultados de las operaciones
     */

	void imprimir();
	
	 /**
      * @brief Retorna los valores de los  float numerador y denominador
      */
	float getNumerador();
	float getDenominador();


private:
	float numerador;   //< Primer parámetro de la fracción.
	float denominador; //< Segundo parámetro de la fracción.
	float resultado;   //< Resultados de cada operación.
};