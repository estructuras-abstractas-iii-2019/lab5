var searchData=
[
  ['operator_2a',['operator*',['../classFraccion.html#ad993ea53924c3efde4fee0f76386a7da',1,'Fraccion::operator*()'],['../classPolinomio.html#a37e31284c67e16879193e8ea1d9d444f',1,'Polinomio::operator*()']]],
  ['operator_2b',['operator+',['../classFraccion.html#a4ac759ff206d99d996897d3e1056205b',1,'Fraccion::operator+()'],['../classPolinomio.html#a8b2ed797af5d211e4622643237061fac',1,'Polinomio::operator+()']]],
  ['operator_2d',['operator-',['../classFraccion.html#abdcd59fe78e7e40c37796d941c946c9c',1,'Fraccion::operator-()'],['../classPolinomio.html#a59a263d2444b8dfe6d474350e5c126de',1,'Polinomio::operator-()']]],
  ['operator_2f',['operator/',['../classFraccion.html#a0fa2661252d5d460e56533c55102e0a3',1,'Fraccion::operator/()'],['../classPolinomio.html#aa803eef2e86e5ecacf7a2b476b599604',1,'Polinomio::operator/()']]],
  ['operator_3d',['operator=',['../classFraccion.html#a9360f581bbad084a9b77a397d7563805',1,'Fraccion::operator=()'],['../classPolinomio.html#a4fc72c320fe9844e411ac63a49ab54c6',1,'Polinomio::operator=()']]]
];
