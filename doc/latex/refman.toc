\contentsline {chapter}{\numberline {1}Class Index}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Class List}{1}{section.1.1}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}Class Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Calculadora$<$ Data $>$ Class Template Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Member Function Documentation}{5}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}add()}{6}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}div()}{6}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}mul()}{6}{subsubsection.3.1.2.3}
\contentsline {subsubsection}{\numberline {3.1.2.4}print()}{7}{subsubsection.3.1.2.4}
\contentsline {subsubsection}{\numberline {3.1.2.5}sub()}{7}{subsubsection.3.1.2.5}
\contentsline {section}{\numberline {3.2}Fraccion Class Reference}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Constructor \& Destructor Documentation}{8}{subsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.2.1}Fraccion()}{8}{subsubsection.3.2.2.1}
\contentsline {subsection}{\numberline {3.2.3}Member Function Documentation}{8}{subsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.3.1}operator$\ast $()}{8}{subsubsection.3.2.3.1}
\contentsline {subsubsection}{\numberline {3.2.3.2}operator+()}{9}{subsubsection.3.2.3.2}
\contentsline {subsubsection}{\numberline {3.2.3.3}operator-\/()}{9}{subsubsection.3.2.3.3}
\contentsline {subsubsection}{\numberline {3.2.3.4}operator/()}{9}{subsubsection.3.2.3.4}
\contentsline {subsubsection}{\numberline {3.2.3.5}operator=()}{10}{subsubsection.3.2.3.5}
\contentsline {section}{\numberline {3.3}Polinomio Class Reference}{10}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{10}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Constructor \& Destructor Documentation}{11}{subsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.2.1}Polinomio()}{11}{subsubsection.3.3.2.1}
\contentsline {subsection}{\numberline {3.3.3}Member Function Documentation}{11}{subsection.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.1}operator$\ast $()}{11}{subsubsection.3.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.3.2}operator+()}{11}{subsubsection.3.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3.3}operator-\/()}{12}{subsubsection.3.3.3.3}
\contentsline {subsubsection}{\numberline {3.3.3.4}operator/()}{12}{subsubsection.3.3.3.4}
\contentsline {subsubsection}{\numberline {3.3.3.5}operator=()}{12}{subsubsection.3.3.3.5}
\contentsline {chapter}{\numberline {4}File Documentation}{13}{chapter.4}
\contentsline {section}{\numberline {4.1}calculadora.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{13}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{14}{subsection.4.1.1}
\contentsline {section}{\numberline {4.2}fraccion.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{14}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{15}{subsection.4.2.1}
\contentsline {section}{\numberline {4.3}polinomio.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}h File Reference}{15}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{15}{subsection.4.3.1}
\contentsline {chapter}{Index}{17}{section*.13}
