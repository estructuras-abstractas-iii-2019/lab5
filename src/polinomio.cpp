#include "polinomio.h"
 
Polinomio::Polinomio(float a, float b, float c){
	this-> a = a;
	this-> b = b;
	this-> c = c;
}
 
Polinomio::~Polinomio(){}


void Polinomio::operator+(const Polinomio &poly){

	resultado[0] = a+poly.a;
	resultado[1] = b+poly.b;
	resultado[2] = c+poly.c;
}

void Polinomio::operator-(const Polinomio &poly){

	resultado[0] = a-poly.a;
	resultado[1] = b-poly.b;
	resultado[2] = c-poly.c;
}

void Polinomio::operator*(const Polinomio &poly){

    resultado[4] = a * poly.a;
    resultado[3] = a * poly.b + b * poly.a; 
    resultado[0] = a * poly.c + b * poly.b + c * poly.a;
    resultado[1] = b * poly.c + c * poly.b;
    resultado[2] = c * poly.c;
}


void Polinomio::operator/(const Polinomio &poly){
    for (int i = -100; i < 100; ++i)
	{
		if (i* poly.b == a){
             d = i*poly.c;
			 e =  b - d;

            for (int j = 0; j < 10; ++j)
	        {
                if(j*poly.b == e){
                    f = j* poly.c;
                    g = c - f;
					cout<<i<<"x"<<"+"<<j<<"+"<<g<<"/"<<"(x"<<poly.b<<"+"<<poly.c<<")"<<endl;

                }
            }
            
		}
    }
}

Polinomio& Polinomio::operator=(const Polinomio &poly){

	this->a = poly.a;
	this->b = poly.b;
	this->c = poly.c;

	return *this;
}
float Polinomio::getA(){return a;}
float Polinomio::getB(){return b;}
float Polinomio::getC(){return c;}

void Polinomio::imprimir(){
	cout<<"El resultado de la operación con polinomios es: "<<endl;
	for (int i = 0; i < 5; ++i)
	{
		if (i==4){
			cout<<resultado[i]<<"x^4"<<endl;
		}
		if (i==3){
			cout<<resultado[i]<<"x^3 + ";
		}
		if (i==0){
			cout<<resultado[i]<<"x^2 + ";
		}
		if (i==1){
			cout<<resultado[i]<<"x + ";
		}
		if (i==2){
			cout<<resultado[i]<<" + ";
		}
	}
}
