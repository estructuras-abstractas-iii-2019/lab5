#include "fraccion.h"
 
Fraccion::Fraccion(float num, float den){
	this->numerador = num;
	this->denominador = den;
}
 
Fraccion::~Fraccion(){}


void Fraccion::operator+(const Fraccion &frac){

	float primera_fraccion = numerador/denominador;
	float segunda_fraccion =  frac.numerador/frac.denominador;

	resultado = primera_fraccion + segunda_fraccion;
}

void Fraccion::imprimir(){
	cout<<"El resultado de la operación con fracciones es: "<<resultado<<endl;
}

void Fraccion::operator-(const Fraccion &frac){

	float primera_fraccion = numerador/denominador;
	float segunda_fraccion =  frac.numerador/frac.denominador;

	resultado = primera_fraccion - segunda_fraccion;
}

void Fraccion::operator*(const Fraccion &frac){

	float primera_fraccion = numerador/denominador;
	float segunda_fraccion =  frac.numerador/frac.denominador;

	resultado = primera_fraccion * segunda_fraccion;
}

void Fraccion::operator/(const Fraccion &frac){

	float primera_fraccion = numerador/denominador;
	float segunda_fraccion =  frac.numerador/frac.denominador;

	resultado = primera_fraccion / segunda_fraccion;
}

Fraccion& Fraccion::operator=(const Fraccion &frac){

	this->numerador = frac.numerador;
	this->denominador = frac.denominador;
	return *this;
}

float Fraccion::getNumerador(){return this->numerador;}
float Fraccion::getDenominador(){return this->denominador;}