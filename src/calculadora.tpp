#include "calculadora.h"
#include "fraccion.h"
#include "polinomio.h"

template <typename Data>
Data Calculadora<Data>::add(Data &d1, const Data &d2){
	d1+d2;
	return d1;
}

template <typename Data>
Data Calculadora<Data>::sub(Data &d1, const Data &d2){
	d1-d2;
	return d1;
}

template <typename Data>
Data Calculadora<Data>::mul(Data &d1, const Data &d2){
	d1*d2;
	return d1;
}

template <typename Data>
Data Calculadora<Data>::div(Data &d1, const Data &d2){
	d1/d2;
	return d1;
}

template <typename Data>
void Calculadora<Data>::print(Data &d1){
	d1.imprimir();
}


template <typename Data>
Calculadora<Data>::Calculadora(){}

template <typename Data>
Calculadora<Data>::~Calculadora(){}