#include "calculadora.h"
#include "fraccion.h"
#include "polinomio.h"

using namespace std;

int main(int argcount, char const *argvalue[])
{
	if (argcount==8){
		
		if (*argvalue[2]=='/' && *argvalue[6]=='/'){
			
			float numer1=atof(argvalue[1]), denom1=atof(argvalue[3]);
			float numer2=atof(argvalue[5]), denom2=atof(argvalue[7]);

			Fraccion fraccion1(numer1, denom1);
			Fraccion fraccion2(numer2, denom2);

			Calculadora<Fraccion> calculoF;
			
			switch (*argvalue[4]){
			
				case '+': 
					calculoF.add(fraccion1,fraccion2);
					calculoF.print(fraccion1);
				break;
			
				case '-': 
					calculoF.sub(fraccion1,fraccion2);
					calculoF.print(fraccion1);
				break;
			
				case 'x': 
					calculoF.mul(fraccion1,fraccion2);
					calculoF.print(fraccion1);
				break;

				case '/': 
					calculoF.div(fraccion1,fraccion2);
					calculoF.print(fraccion1);
				break;

				case '=': 
					cout<<"La fracción "<<numer1<<"/"<<denom1<<" se transformó a: ";
					cout<<(fraccion1=fraccion2).getNumerador()<<"/"<<(fraccion1=fraccion2).getDenominador()<<endl;
				break;
        		
        		default: cout << "Operación no válida" << endl;
        				 return 1;
        	}
				
		}

		else{
			float a1=atof(argvalue[1]), b1=atof(argvalue[2]), c1=atof(argvalue[3]);
			float a2=atof(argvalue[5]), b2=atof(argvalue[6]), c2=atof(argvalue[7]);
			
			Polinomio polinomio1(a1,b1,c1);
			Polinomio polinomio2(a2,b2,c2);

			Calculadora<Polinomio> calculoP;


			switch (*argvalue[4]){
			
				case '+': 
					calculoP.add(polinomio1,polinomio2);
					calculoP.print(polinomio1);
				break;
			
				case '-': 
					calculoP.sub(polinomio1,polinomio2);
					calculoP.print(polinomio1);
				break;
			
				case 'x': 
					calculoP.mul(polinomio1,polinomio2);
					calculoP.print(polinomio1);
				break;

				case '/': 

					if (a1==0 && b1==0 && c1==0){
						cout<<"El resultado es cero"<<endl;
					}else if(a2==0 && b2==0 && c2==0){
						cout<<"Operación inválida, división entre cero"<<endl;
						return 1;
					} else{
						cout<<"El resultado de la operación con polinomios es: "<<endl;
						calculoP.div(polinomio1,polinomio2);
					}
				break;

				case '=': 
					cout<<"El polinomio "<<polinomio1.getA()<<"x^2 + "<<polinomio1.getB()<<"x + "<<polinomio1.getC()<<" se transformó a: "<<endl;
					cout<<(polinomio1=polinomio2).getA()<<"x^2 + "<<(polinomio1=polinomio2).getB()<<"x + "<<(polinomio1=polinomio2).getC()<<endl;
				break;
        		
        		default: cout << "Operación no válida" << endl;
        				 return 1;
        	}
        }
	}

	return 0;
}